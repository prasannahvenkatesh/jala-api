package com.example.APITesting_Postman.CONTROLLER;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.APITesting_Postman.BEAN.DemoForJobSeekerBean;
import com.example.APITesting_Postman.SERVICE.DemoForJobSeekerServiceIMPL;



/*
 * Controller acts as bridge between database/bean and webpage, It accepts the url and connect with repective function with the help of RequestMapping annotation.
 */
@RestController
public class DemoForJobSeekerController {
	
	@Autowired
	DemoForJobSeekerServiceIMPL service;
	
	@RequestMapping(value="/demosearch{s}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public DemoForJobSeekerBean findStudent(@PathVariable("s") String value) {
		DemoForJobSeekerBean sbean = service.search(value);
		return sbean;
	}
}
