package com.example.APITesting_Postman.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.APITesting_Postman.ENTITY.DemoForJobSeekerEntity;


/*
 * DAO Interface is for performing various operation and mandatory function like save,delete are done by extending JpaRepository.
 * We can also add our user specific operation as shown below using Query. 
 */

@Repository
@Transactional
public interface DemoForJobSeekerDAO extends JpaRepository<DemoForJobSeekerEntity,Integer>{
	
	@Query("select s from DemoForJobSeekerEntity s where (UPPER(s.emailId)=UPPER(?1) or UPPER(s.mobileNumber)=UPPER(?1))")
	public DemoForJobSeekerEntity findBySearch(String value);
}
