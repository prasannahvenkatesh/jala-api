package com.example.APITesting_Postman.SERVICE;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.APITesting_Postman.BEAN.DemoForJobSeekerBean;
import com.example.APITesting_Postman.DAO.DemoForJobSeekerDAO;
import com.example.APITesting_Postman.ENTITY.DemoForJobSeekerEntity;



/* In Service class, The methods in DAO are called and it acts as bridge between DAO and UserController class. 
 * Convertion of Entity to Bean and viceversa are done by using BeanUtils methods
 * */
@Service
@Transactional
public class DemoForJobSeekerServiceIMPL implements DemoForJobSeekerService{
	
	@Autowired
	DemoForJobSeekerDAO dao;
	
	@PersistenceContext
	EntityManager em;
	
	public DemoForJobSeekerBean search(String value){
		DemoForJobSeekerEntity sentity = dao.findBySearch(value);
		DemoForJobSeekerBean sbean = new DemoForJobSeekerBean();
		BeanUtils.copyProperties(sentity, sbean);
		return sbean;
	}
	
}
