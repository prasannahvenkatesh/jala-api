package com.example.APITesting_Postman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTesting_PostmanApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTesting_PostmanApplication.class, args);
	}

}
