**API Postman Setup **

1. Clone the Project to Eclipse IDE. 
2. Run the project, Right-click on project file -> Run As -> Java Application.
3. Before that please chek some data available on server.
4. Open the Postman and Set Request as GET and use link - http://localhost:8080/search. In link after search add phonenumber or emailId, you want to search 
for eg: http://localhost/8080/search9876543210. 
5. Click Run. You will get Output in the form of json.

**SOAP API ** 

1. Clone the Project to Eclipse IDE. 
2. Run the project, Right-click on project file -> Run As -> Run on Server.
3. Select both the file SOAP_API and SOAP_APIClient.
4. Before that please chek some data available on server. 
5. Use link to test : http://localhost:8080/SOAP_APIClient/sampleSearchSOAPProxy/TestClient.jsp?endpoint=http://localhost:5854/SOAP_API/services/SearchSOAP
6. Click getSearch() method and enter mobile number or emailId and click invoke. You will get outptu in form of array.
