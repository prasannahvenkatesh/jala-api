/**
 * SearchSOAPServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.soapapitesting;

public class SearchSOAPServiceLocator extends org.apache.axis.client.Service implements com.soapapitesting.SearchSOAPService {

    public SearchSOAPServiceLocator() {
    }


    public SearchSOAPServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SearchSOAPServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SearchSOAP
    private java.lang.String SearchSOAP_address = "http://localhost:8080/SOAP_API/services/SearchSOAP";

    public java.lang.String getSearchSOAPAddress() {
        return SearchSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SearchSOAPWSDDServiceName = "SearchSOAP";

    public java.lang.String getSearchSOAPWSDDServiceName() {
        return SearchSOAPWSDDServiceName;
    }

    public void setSearchSOAPWSDDServiceName(java.lang.String name) {
        SearchSOAPWSDDServiceName = name;
    }

    public com.soapapitesting.SearchSOAP getSearchSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SearchSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSearchSOAP(endpoint);
    }

    public com.soapapitesting.SearchSOAP getSearchSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.soapapitesting.SearchSOAPSoapBindingStub _stub = new com.soapapitesting.SearchSOAPSoapBindingStub(portAddress, this);
            _stub.setPortName(getSearchSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSearchSOAPEndpointAddress(java.lang.String address) {
        SearchSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.soapapitesting.SearchSOAP.class.isAssignableFrom(serviceEndpointInterface)) {
                com.soapapitesting.SearchSOAPSoapBindingStub _stub = new com.soapapitesting.SearchSOAPSoapBindingStub(new java.net.URL(SearchSOAP_address), this);
                _stub.setPortName(getSearchSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SearchSOAP".equals(inputPortName)) {
            return getSearchSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://soapapitesting.com", "SearchSOAPService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://soapapitesting.com", "SearchSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SearchSOAP".equals(portName)) {
            setSearchSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
