package com.soapapitesting;

public class SearchSOAPProxy implements com.soapapitesting.SearchSOAP {
  private String _endpoint = null;
  private com.soapapitesting.SearchSOAP searchSOAP = null;
  
  public SearchSOAPProxy() {
    _initSearchSOAPProxy();
  }
  
  public SearchSOAPProxy(String endpoint) {
    _endpoint = endpoint;
    _initSearchSOAPProxy();
  }
  
  private void _initSearchSOAPProxy() {
    try {
      searchSOAP = (new com.soapapitesting.SearchSOAPServiceLocator()).getSearchSOAP();
      if (searchSOAP != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)searchSOAP)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)searchSOAP)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (searchSOAP != null)
      ((javax.xml.rpc.Stub)searchSOAP)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.soapapitesting.SearchSOAP getSearchSOAP() {
    if (searchSOAP == null)
      _initSearchSOAPProxy();
    return searchSOAP;
  }
  
  public java.lang.String[] search(java.lang.String value) throws java.rmi.RemoteException{
    if (searchSOAP == null)
      _initSearchSOAPProxy();
    return searchSOAP.search(value);
  }
  
  
}