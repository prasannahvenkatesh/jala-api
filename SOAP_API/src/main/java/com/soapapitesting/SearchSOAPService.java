/**
 * SearchSOAPService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.soapapitesting;

public interface SearchSOAPService extends javax.xml.rpc.Service {
    public java.lang.String getSearchSOAPAddress();

    public com.soapapitesting.SearchSOAP getSearchSOAP() throws javax.xml.rpc.ServiceException;

    public com.soapapitesting.SearchSOAP getSearchSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
