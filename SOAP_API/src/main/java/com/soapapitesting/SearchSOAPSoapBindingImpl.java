/**
 * SearchSOAPSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.soapapitesting;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

// link to test : http://localhost:8080/SOAP_APIClient/sampleSearchSOAPProxy/TestClient.jsp?endpoint=http://localhost:5854/SOAP_API/services/SearchSOAP
public class SearchSOAPSoapBindingImpl implements com.soapapitesting.SearchSOAP{
    public java.lang.String[] search(java.lang.String value) throws java.rmi.RemoteException {
    	String[] s = new String[12];
    	try{  
    		Class.forName("com.mysql.jdbc.Driver");  
    		Connection con=DriverManager.getConnection(  
    		"jdbc:mysql://localhost:3306/API_TESTING_DB?characterEncoding=utf8","root","root");  
    		//here sonoo is database name, root is username and password  
    		Statement stmt=con.createStatement();  
    		ResultSet rs=stmt.executeQuery("select * from jobseeker");  
    		while(rs.next())  
    			if(rs.getString(4).equalsIgnoreCase(value) || rs.getString(10).equalsIgnoreCase(value)) {
    				for(int i=1;i<=12;i++) {
    					s[i-1] = rs.getString(i);
    				}
    			}
    		con.close();  
    		}catch(Exception e){ System.out.println(e);}
    	return s;
    		}  
        
    }

